<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function () use ($router) {
     // Matches "/api/register
    $router->post('register', 'AuthController@register');

      // Matches "/api/login
     $router->post('login', 'AuthController@login');
});

//route posts table
$router->get('posts', 'PostController@index');
$router->post("posts/insert", "PostController@store");
$router->put("posts/update/{id}", "PostController@update");
$router->delete("posts/{id}", "PostController@destroy");

//route company table
$router->get("company", "JojoController@index");
$router->post('company/insert', ['middleware' => 'test', 'uses' =>  'JojoController@store']);
//$router->post("company/insert", "Auth/JojoController@store")->middleware('BeforeMiddleware');
//$router->post("company/insert", "JojoController@store")
$router->put("company/update/{id}", "JojoController@update");
$router->delete("company/delete/{id}", "JojoController@destroy");


//route company package table
$router->get('company_package', 'CompanyPackageController@index');
$router->post("company_package/insert", "CompanyPackageController@store");
$router->put("company_package/update/{id}", "CompanyPackageController@update");
$router->delete("company_package/delete/{id}", "CompanyPackageController@destroy");

//route query company package table
$router->get('companyquery', 'JojoController@query');
$router->get('company_package_query', 'JojoController@company_package_query');


//route user access
$router->get('/', function () use ($router) {
  $response['success'] = true;
  $response['result'] = "Hello there welcome to web api Jojonomic!";
  return response($response);
});$router->post("login", 'LoginController@index');
$router->post("register", 'UserController@register');
$router->get("user/{id}", ['middleware' => 'auth', 'uses' =>  'UserController@get_user']);