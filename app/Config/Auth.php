//config.auth.php

<?php

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'users',
    ],

    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'users',
        ],
    ],

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => \App\User::class
        ]
    ]
	
   /*'guards' => [
   'web' => [
      'driver' => 'session',
      'provider' => 'users',
   ],

   'api' => [
       'driver' => 'passport',
       'provider' => 'users',
   ]*/

];

