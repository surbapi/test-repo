<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyPackage extends Model
{
	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'updated_date';
    protected $table = "company_package";
	protected $fillable = [
        'company_id', 
		'code',
		'created_date',
		'updated_date',
		'expired_date',
		'state',
		'is_default'
    ];
	
	public function company()
    {
        return $this->belongsTo('App\Models\Company', 'id', 'company_id');
    }
	
}