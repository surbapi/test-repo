<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'updated_date';
    protected $table = "company";
	protected $fillable = [
        'account_id', 
		'name',
		'phone',
		'phone_finance',
		'email',
		'country',
		'country_code',
		'npwp',
		'npwp_date',
		'bpjs',
		'registered_name',
		'city',
		'province',
		'industry',
		'address',
		'description',
		'photo_url',
		'web_url',
		'quota_user',
		'code',
		'start_date',
		'expired_date',
		'start_trial_date',
		'end_trial_date',
		'start_pay_date',
		'created_type',
		'is_active',
		'pic_note',
		'created_date',
		'currency_code',
		'currency_symbol',
		'timezone',
		'is_lock_location',
		'is_send_email_staff',
		'is_direct_bank_transfer',
		'is_employee_receivable',
		'is_external_user',
		'is_jojocard',
		'receive_url_post_reimbursement',
		'bank_name',
		'bank_account_number',
		'bank_account_name',
		'state',
		'size',
		'version'
    ];
	
	public function companyPackage()
    {
        return $this->Hasmany('App\Models\CompanyPackage')->select('id','company_id');
    }
}