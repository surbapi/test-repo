<?php

namespace App\Http\Controllers;

use App\Models\Company;
use app\Models\CompanyPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;
use illuminate\Support\Facades\DB;

class JojoController extends Controller
{
	
	//view data
	public function index()
    {

		//$getPost = DB::select("SELECT * from company")->paginate(10);
		//$getPost = app('db')->select("SELECT * FROM company");
		$getPost = Company::paginate(10);
		
        $out = [
            "message" => "list_post",
            "results" => $getPost
        ];
 
        return response()->json($out, 200);	
    }
	
	
	//add data
	public function store(Request $request)
    {
 
        if ($request->isMethod('post')) {
 
            $this->validate($request, [
                'account_id' => 'required',
                'name' => 'required'
				//'email' => 'required',
				//'phone' => 'required',
				//'phone_finance' => 'required',
				//'country' => 'required',
				//'country_code' => 'required'
            ]);
 			
			$account_id = $request->input('account_id');
            $email = $request->input('email');
			$name = $request->input('name');
			$phone = $request->input('phone');
			$phone_finance = $request->input('phone_finance');
			$email = $request->input('email');
			$country = $request->input('country');
			$country_code = $request->input('country_code');
			$npwp = $request->input('npwp');
			$npwp_date = $request->input('npwp_date');
			$bpjs = $request->input('bpjs');
			$registered_name = $request->input('registered_name');
			$city = $request->input('city');
			$province = $request->input('province');
			$industry = $request->input('industry');
			$address = $request->input('address');
			$description = $request->input('description');
			$photo_url = $request->input('photo_url');
			$web_url = $request->input('web_url');
			$quota_user = $request->input('quota_user');
			$code = $request->input('code');
			$start_date = $request->input('start_date');
			$expired_date = $request->input('expired_date');
			$start_trial_date = $request->input('end_trial_date');
			$end_trial_date = $request->input('end_trial_date');
			$start_pay_date = $request->input('start_pay_date');
			$created_type = $request->input('created_type');
			$is_active = $request->input('is_active');
			$pic_note = $request->input('pic_note');
			$created_date = $request->input('created_date');
			$updated_date = $request->input('updated_date');
			$currency_code = $request->input('currency_code');
			$currency_symbol = $request->input('currency_symbol');
			$timezone = $request->input('timezone');
			$is_lock_location = $request->input('is_lock_location');
			$is_send_email_staff = $request->input('is_send_email_staff');
			$is_direct_bank_transfer = $request->input('is_direct_bank_transfer');
			$is_employee_receivable = $request->input('is_employee_receivable');
			$is_external_user = $request->input('is_external_user');
			$is_jojocard = $request->input('is_jojocard');
			$receive_url_post_reimbursement = $request->input('receive_url_post_reimbursement');
			$bank_name = $request->input('bank_name');
			$bank_account_number = $request->input('bank_account_number');
			$bank_account_name = $request->input('bank_account_name');
			$state = $request->input('state');
			$size = $request->input('size');
			$version = $request->input('version');

			
			
			//$data = $request->all();
 
            $data = [
                'account_id' => $account_id,
				'name' => $name,
				'phone' => $phone,
				'phone_finance' => $phone_finance,
				'email' => $email,
				'country' => $country,
				'country_code' => $country_code,
				'npwp' => $npwp,
				'npwp_date' => $npwp_date,
				'bpjs' => $bpjs,
				'registered_name' => $registered_name,
				'city' => $city,
				'province' => $province,
				'industry' => $industry,
				'address' => $address,
				'description' => $description,
				'photo_url' => $photo_url,
				'web_url' => $web_url,
				'quota_user' => $quota_user,
				'code' => $code,
				'start_date' => $start_date,
				'expired_date' => $expired_date,
				'start_trial_date' => $start_trial_date,
				'end_trial_date' => $end_trial_date,
				'start_pay_date' => $start_pay_date,
				'created_type' => $created_type,
				'is_active' => $is_active,
				'pic_note' => $pic_note,
				'created_date' => $created_date,
				'updated_date' => $updated_date,
				'currency_code'=> $currency_code,
				'currency_symbol' => $currency_symbol,
				'timezone' => $timezone,
				'is_lock_location' => $is_lock_location,
				'is_send_email_staff' => $is_send_email_staff,
				'is_direct_bank_transfer' => $is_direct_bank_transfer,
				'is_employee_receivable' => $is_employee_receivable,
				'is_external_user' => $is_external_user,
				'is_jojocard' => $is_jojocard,
				'receive_url_post_reimbursement' => $receive_url_post_reimbursement,
				'bank_name' => $bank_name,
				'bank_account_number' => $bank_account_number,
				'bank_account_name' => $bank_account_name,
				'state' => $state,
				'size' => $size,
				'version' => $version	
            ];
 
            $insert = Company::create($data);
 
            if ($insert) {
                $out  = [
                    "message" => "success_insert_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "failed_insert_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }
 
            return response()->json($out, $out['code']);
        }
    }
	
	//update data
	public function update($id, Request $request)
    {
 
        if ($request->isMethod('put')) {
 
            $this->validate($request, [
                'account_id' => 'required',
                'email'  => 'required',
                'id'    => 'required'
            ]);
            $id = $request->input('id');
            $account_id = $request->input('account_id');
            $email = $request->input('email');
			$name = $request->input('name');
			$phone = $request->input('phone');
			$phone_finance = $request->input('phone_finance');
			$email = $request->input('email');
			$country = $request->input('country');
			$country_code = $request->input('country_code');
			$npwp = $request->input('npwp');
			$npwp_date = $request->input('npwp_date');
			$bpjs = $request->input('bpjs');
			$registered_name = $request->input('registered_name');
			$city = $request->input('city');
			$province = $request->input('province');
			$industry = $request->input('industry');
			$address = $request->input('address');
			$description = $request->input('description');
			$photo_url = $request->input('photo_url');
			$web_url = $request->input('web_url');
			$quota_user = $request->input('quota_user');
			$code = $request->input('code');
			$start_date = $request->input('start_date');
			$expired_date = $request->input('expired_date');
			$start_trial_date = $request->input('end_trial_date');
			$end_trial_date = $request->input('end_trial_date');
			$start_pay_date = $request->input('start_pay_date');
			$created_type = $request->input('created_type');
			$is_active = $request->input('is_active');
			$pic_note = $request->input('pic_note');
			$created_date = $request->input('created_date');
			$updated_date = $request->input('updated_date');
			$currency_code = $request->input('currency_code');
			$currency_symbol = $request->input('currency_symbol');
			$timezone = $request->input('timezone');
			$is_lock_location = $request->input('is_lock_location');
			$is_send_email_staff = $request->input('is_send_email_staff');
			$is_direct_bank_transfer = $request->input('is_direct_bank_transfer');
			$is_employee_receivable = $request->input('is_employee_receivable');
			$is_external_user = $request->input('is_external_user');
			$is_jojocard = $request->input('is_jojocard');
			$receive_url_post_reimbursement = $request->input('receive_url_post_reimbursement');
			$bank_name = $request->input('bank_name');
			$bank_account_number = $request->input('bank_account_number');
			$bank_account_name = $request->input('bank_account_name');
			$state = $request->input('state');
			$size = $request->input('size');
			$version = $request->input('version');
 
            $post = Company::find($id);
            $data = [
                'account_id' => $account_id,
				'name' => $name,
				'phone' => $phone,
				'phone_finance' => $phone_finance,
				'email' => $email,
				'country' => $country,
				'country_code' => $country_code,
				'npwp' => $npwp,
				'npwp_date' => $npwp_date,
				'bpjs' => $bpjs,
				'registered_name' => $registered_name,
				'city' => $city,
				'province' => $province,
				'industry' => $industry,
				'address' => $address,
				'description' => $description,
				'photo_url' => $photo_url,
				'web_url' => $web_url,
				'quota_user' => $quota_user,
				'code' => $code,
				'start_date' => $start_date,
				'expired_date' => $expired_date,
				'start_trial_date' => $start_trial_date,
				'end_trial_date' => $end_trial_date,
				'start_pay_date' => $start_pay_date,
				'created_type' => $created_type,
				'is_active' => $is_active,
				'pic_note' => $pic_note,
				'created_date' => $created_date,
				'updated_date' => $updated_date,
				'currency_code'=> $currency_code,
				'currency_symbol' => $currency_symbol,
				'timezone' => $timezone,
				'is_lock_location' => $is_lock_location,
				'is_send_email_staff' => $is_send_email_staff,
				'is_direct_bank_transfer' => $is_direct_bank_transfer,
				'is_employee_receivable' => $is_employee_receivable,
				'is_external_user' => $is_external_user,
				'is_jojocard' => $is_jojocard,
				'receive_url_post_reimbursement' => $receive_url_post_reimbursement,
				'bank_name' => $bank_name,
				'bank_account_number' => $bank_account_number,
				'bank_account_name' => $bank_account_name,
				'state' => $state,
				'size' => $size,
				'version' => $version
            ];
 
            $update = $post->update($data);
 
            if ($update) {
                $out  = [
                    "message" => "success_update_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "failed_update_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }
 
            return response()->json($out, $out['code']);
        }
    }
	
	
	//delete data
	public function destroy($id)
    {
        $posts =  Company::find($id);
 
        if (!$posts) {
            $data = [
                "message" => "id not found",
            ];
        } else {
            $posts->delete();
            $data = [
                "message" => "success_deleted"
            ];
        }
 
        return response()->json($data, 200);
    }
	
	//view data query
	public function query()
    {

		//$getPost = DB::select("SELECT * from company")->paginate(10);
		/*$getPost = app('db')->select("SELECT a.id, 
									a.account_id,
									b.company_id,
									b.code
									FROM company A join company_package B on a.id = b.company_id");
		//$getPost = Company::paginate(10);*/
		//dd($getPost);
		$getPost = Company::select("*")->with("companyPackage")->get();
        $out = [
            "message" => "list_post",
            "results" => $getPost->toArray()
        ];
 
        return response()->json($out, 200);	
    }
	
}