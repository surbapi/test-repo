<?php

namespace App\Http\Controllers;

use App\Models\CompanyPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;
use illuminate\Support\Facades\DB;

class CompanyPackageController extends Controller
{
	
	//view data
	public function index()
    {

		//$getPost = DB::select("SELECT * from company_package");
		//$getPost = app('db')->select("SELECT * FROM company");
		$getPost = CompanyPackage::paginate(10);
		
        $out = [
            "message" => "list_post",
            "results" => $getPost
        ];
 
        return response()->json($out, 200);	
    }
	
	
	//add data
	public function store(Request $request)
    {
 
        if ($request->isMethod('post')) {
 
            $this->validate($request, [
                'company_id' => 'required',
                'code' => 'required'
				//'email' => 'required',
				//'phone' => 'required',
				//'phone_finance' => 'required',
				//'country' => 'required',
				//'country_code' => 'required'
            ]);
 
            $company_id = $request->input('company_id');
            $code = $request->input('code');
			//$email = $request->input('email');
			//$phone = $request->input('phone');
			//$phone_finance = $request->input('phone_finance');
			//$country = $request->input('country');
			//$country_code =$request->input('country_code');
			
			
			//$data = $request->all();
 
            $data = [
                'company_id' => $company_id,
				'code' => $code
				//'email' => $email,
				//'phone' => $phone,
				//'phone_finance' => $phone_finance,
				//'country' => $country,
				//'country_code' => $country_code
            ];
 
            $insert = CompanyPackage::create($data);
 
            if ($insert) {
                $out  = [
                    "message" => "success_insert_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "failed_insert_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }
 
            return response()->json($out, $out['code']);
        }
    }
	
	//update data
	public function update($id, Request $request)
    {
 
        if ($request->isMethod('put')) {
 
            $this->validate($request, [
                'company_id' => 'required',
                'code'  => 'required',
                'id'    => 'required'
            ]);
            $id = $request->input('id');
            $company_id = $request->input('company_id');
            $code = $request->input('code');
 
            $post = CompanyPackage::find($id);
 
            $data = [
                'company_id' => $company_id,
                'code' => $code
            ];
 
            $update = $post->update($data);
 
            if ($update) {
                $out  = [
                    "message" => "success_update_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "failed_update_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }
 
            return response()->json($out, $out['code']);
        }
    }
	
	
	//delete data
	public function destroy($id)
    {
        $posts =  CompanyPackage::find($id);
 
        if (!$posts) {
            $data = [
                "message" => "id not found",
            ];
        } else {
            $posts->delete();
            $data = [
                "message" => "success_deleted"
            ];
        }
 
        return response()->json($data, 200);
    }

}