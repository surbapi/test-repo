<?php

namespace App\Http\Middleware;

use Closure;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if ($request->input('state') = 1) {
        //    return redirect('company/insert');
        //}
		
		$message = 'middleware success';
		
        return $next($request, $message);
    }
	
	public function terminate($request, $response)
    {
		return redirect('company');

    }
}
