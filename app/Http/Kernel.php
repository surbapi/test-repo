<?php
protected $routeMiddleware = [
    'test' => \App\Http\Middleware\BeforeMiddleware::class,
	'auth' => \App\Http\Middleware\Authenticate::class,	
];
